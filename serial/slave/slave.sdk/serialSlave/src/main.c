#include "../include/uartFunc.h"
//#include "../include/psLed.h"

int main() {
	u32 ledR = 52;
	u32 ledG = 53;

	XGpioPs Gpio;

	XGpioPs_Config *GPIOConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	sensorData sensorParameters = {0};

	sensorParameters.tempProbes = TPROBES;
	sensorParameters.eitProbes = IPROBES;
	sensorParameters.ectProbes = CPROBES;
	sensorParameters.specProbes = SPROBES;

	initLed(&Gpio, GPIOConfigPtr, ledG, ledR);
	XGpioPs_WritePin(&Gpio, ledG, 0);
	XGpioPs_WritePin(&Gpio, ledR, 0);
	initUart(UART_DEVICE_ID);

	union byteFloat dummyData[3];
	dummyData[0].floatVal = 10.8;
	dummyData[1].floatVal = 1.23;
	dummyData[2].floatVal = 90.1;

	if(initParam(&sensorParameters, UART_DEVICE_ID)) {
		return XST_FAILURE;
	}

	while(!dataTransfer(&sensorParameters, UART_DEVICE_ID, dummyData)) {

		//return XST_SUCCESS;
	}
	return XST_SUCCESS;
}
