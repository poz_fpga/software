#include "../include/psLed.h"

int8_t initLed(XGpioPs* Gpio, XGpioPs_Config *GPIOConfigPtr, u32 ledG, u32 ledR) {
	int8_t status;

	status = XGpioPs_CfgInitialize(Gpio, GPIOConfigPtr,GPIOConfigPtr->BaseAddr);


	if (status != XST_SUCCESS) {
		printf("status error \n\r");
		return XST_FAILURE;
	}

	XGpioPs_SetDirectionPin(Gpio, ledR, 1);
	XGpioPs_SetOutputEnablePin(Gpio, ledR, 1);
	XGpioPs_SetDirectionPin(Gpio, ledG, 1);
	XGpioPs_SetOutputEnablePin(Gpio, ledG, 1);
	return XST_SUCCESS;
}
