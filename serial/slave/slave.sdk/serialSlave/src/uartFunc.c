#include "../include/uartFunc.h"

int8_t initUart(uint16_t device) {
	int8_t Status;
	XUartPs_Config *Config;

	Config = XUartPs_LookupConfig(device);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XUartPs_CfgInitialize(&Uart_PS, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XUartPs_SetBaudRate(&Uart_PS, 115200);

	/* Check hardware build. */
	Status = XUartPs_SelfTest(&Uart_PS);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* Use local loopback mode. */
	XUartPs_SetOperMode(&Uart_PS, XUARTPS_OPER_MODE_NORMAL);

	return XST_SUCCESS;
}

u32 read32int() {
	uint8_t recCnt = 0;
	union byte32Int buffer;
	buffer.uIntVal = 0;
	while(recCnt < INT_BUFFER_SIZE) {
		recCnt += XUartPs_Recv(&Uart_PS, &buffer.byteVal[recCnt],
				INT_BUFFER_SIZE - recCnt);
	}
	return buffer.uIntVal;
}

u8 readByte() {
	uint8_t recCnt = 0;
	u8 buffer = 0;
	while(recCnt < BYTE_BUFFER_SIZE) {
		recCnt += XUartPs_Recv(&Uart_PS, &buffer,
				BYTE_BUFFER_SIZE - recCnt);
	}
	return buffer;
}

u8 writeByte(u8 byte) {
	u8 sendCount = XUartPs_Send(&Uart_PS, &byte, BYTE_BUFFER_SIZE);
	while(XUartPs_IsSending(&Uart_PS));
	if (sendCount != BYTE_BUFFER_SIZE) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

u8 writeFloat(union byteFloat data) {
	u8 sendCount = XUartPs_Send(&Uart_PS, data.byteVal, FLOAT_BUFFER_SIZE);
	while(XUartPs_IsSending(&Uart_PS));
	if(sendCount != FLOAT_BUFFER_SIZE) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;

}

int8_t initParam(sensorData* sensorParam, uint16_t device) {
	u8 finishFlag = 0;
	//u8 sendByteBuf = 0;
	u8 recByteBuf = 0;
	//u32 intBuf = 0;

	//initPrefix prefixes = PREFIX_DEFAULTS;
	ackBytes ackBytes = ACKBYTE_DEFAULTS;

	while(ackBytes.start != (recByteBuf = readByte()));

	recByteBuf = '\0';
	//sendByteBuf = ackBytes.ok;

	if(writeByte(ackBytes.ok)) {
		return XST_FAILURE;
	}
	while(XUartPs_IsSending(&Uart_PS));

	while(!finishFlag) {
		recByteBuf = readByte();
		writeByte(ackBytes.ok);
		switch(recByteBuf) {
		case 'T':
			if(sensorParam->tempProbes != read32int()) {
				writeByte(ackBytes.nok);
				return XST_FAILURE;
			}
			writeByte(ackBytes.ok);
			break;
		case 'I':
			if(sensorParam->eitProbes != read32int()) {
				writeByte(ackBytes.nok);
				return XST_FAILURE;
			}
			writeByte(ackBytes.ok);
			break;
		case 'C':
			if(sensorParam->ectProbes != read32int()) {
				writeByte(ackBytes.nok);
				return XST_FAILURE;
			}
			writeByte(ackBytes.ok);
			break;
		case 'S':
			if(sensorParam->specProbes != read32int()) {
				writeByte(ackBytes.nok);
				return XST_FAILURE;
			}
			writeByte(ackBytes.ok);
			break;
		case 'V':
			sensorParam->interval = read32int();
			writeByte(ackBytes.ok);
			break;
		case 'F':
			finishFlag = 1;
			break;
		default:
			break;
		}
	}

	return XST_SUCCESS;
}

int8_t dataTransfer(sensorData* sensorParam, u16 device, union byteFloat* dummyData) {
	ackBytes ackBytes = ACKBYTE_DEFAULTS;
	initPrefix prefixes = PREFIX_DEFAULTS;
	//u8 readIndex = 0;

	while(1) {
		writeByte(START);
		if(ackBytes.ok != readByte()) {
			return XST_FAILURE;
		}
		for(u8 i = 0; i < TPROBES; i++) {
			writeByte(prefixes.temp);
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
			if(writeFloat(dummyData[i%3])) {
				return XST_FAILURE;
			}
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
		}

		for(u8 i = 0; i < IPROBES; i++) {
			writeByte(prefixes.eit);
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
			if(writeFloat(dummyData[i%3])) {
				return XST_FAILURE;
			}
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
		}

		for(u8 i = 0; i < CPROBES; i++) {
			writeByte(prefixes.ect);
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
			if(writeFloat(dummyData[i%3])) {
				return XST_FAILURE;
			}
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
		}

		for(u8 i = 0; i < SPROBES; i++) {
			writeByte(prefixes.spec);
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
			if(writeFloat(dummyData[i%3])) {
				return XST_FAILURE;
			}
			if(ackBytes.ok != readByte()) {
				return XST_FAILURE;
			}
		}

		writeByte(ackBytes.finished);
		if(ackBytes.ok != readByte()) {
			return XST_FAILURE;
		}
		usleep_A9(sensorParam->interval);
		//usleep_A9(100);
	}
	return XST_SUCCESS;
}
