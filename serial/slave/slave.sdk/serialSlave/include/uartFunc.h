#ifndef UARTF
#define UARTF

#include "xparameters.h"
#include "xuartps.h"
#include "xil_printf.h"
#include "psLed.h"
#include "dataStruct.h"
#include "sleep.h"

#define UART_DEVICE_ID              XPAR_XUARTPS_0_DEVICE_ID
#define FINISH 'F'
#define START 'S'

XUartPs Uart_PS;

int8_t initUart(uint16_t device);
u32 read32int();
u8 readByte();
u8 writeByte(u8 byte);
u8 writeFloat(union byteFloat data);
int8_t initParam(sensorData* sensorParam, u16 device);
int8_t dataTransfer(sensorData* sensorParam, u16 device, union byteFloat* dummyData);
#endif
