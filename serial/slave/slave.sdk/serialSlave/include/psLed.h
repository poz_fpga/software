#ifndef PSLED
#define PSLED

#include <stdio.h>
#include "xparameters.h"
#include "xgpiops.h"
#include "xstatus.h"
#include "sleep.h"

int8_t initLed(XGpioPs* Gpio, XGpioPs_Config *GPIOConfigPtr, u32 ledG, u32 ledR);

#endif /* INCLUDE_PSLED_H_ */
