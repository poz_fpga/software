#ifndef DATASTRUCT
#define DATASTRUCT
#include "xil_types.h"

#define BAUDRATE 115200u
#define TPROBES 10u
#define IPROBES 12u
#define CPROBES 15u
#define SPROBES 5u

#define ACKBYTE_DEFAULTS { 'O', 'N', 'F', 'S' };
#define PREFIX_DEFAULTS { 'T', 'I', 'C', 'S' };

#define BYTE_BUFFER_SIZE 1
#define INT_BUFFER_SIZE 4
#define FLOAT_BUFFER_SIZE 4

typedef struct ackBytes {
	const uint8_t ok;
	const uint8_t nok;
	const uint8_t finished;
	const uint8_t start;
} ackBytes;

typedef struct initPrefix {
	const uint8_t temp;
	const uint8_t eit;
	const uint8_t ect;
	const uint8_t spec;
} initPrefix;

union byte32Int {
		u32 uIntVal;
		u8 byteVal[INT_BUFFER_SIZE];
} byte32Int;

union byteFloat {
		float floatVal;
		u8 byteVal[FLOAT_BUFFER_SIZE];
} byteFloat;

typedef struct sensorData {
	uint32_t baudRate;
	uint32_t reads;
	uint32_t interval;
	uint32_t tempProbes;
	uint32_t eitProbes;
	uint32_t ectProbes;
	uint32_t specProbes;
	} sensorData;
#endif
