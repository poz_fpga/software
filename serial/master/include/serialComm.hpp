#ifndef SERIAL
#define SERIAL
#include <iostream>
#include <string>
#include <memory>
#include <fstream>
#include <filesystem>
#include "nlohmann/json.hpp"
#include "fpgaData.hpp"

#define CHECKPROBE(name)				\
    if(ackBytes.ok != readByte()) {			\
    std::cout << "Probe " << name << " mismatch\n";	\
    return EXIT_FAILURE;}


namespace serialComm {
    class sCommClass {
    public:
	sCommClass(std::string portIn, uint32_t baudRate, uint32_t timeOut)
		: sCommDev(portIn, baudRate, serial::Timeout::simpleTimeout(timeOut)),
		  sCommData(),
		  ackBytes(), recBuffer(), sendBuffer(), finishFlag{0} {
	    // Check if the port is open
	    if (sCommDev.isOpen()) {
		printf("Serial port opened successfully.\n");
	    } else {
		exit(EXIT_SUCCESS);
	    }
	}
	~sCommClass() {}

	[[nodiscard]] inline auto initParam(fpgaData::argData paramData) noexcept -> uint8_t {
	    writeByte(ackBytes.start);
	    //CHECKPROBE("start");
            if(ackBytes.ok != readByte()) {
                std::cout << "Cannot start." << std::endl;
                return EXIT_FAILURE;
            }
	    printf("Start ok.\n");

	    writeByte(byteDef::TEMPPREFIX<uint8_t>);
	    write32Int(paramData.tempProbes);
	    CHECKPROBE("temp");
	    printf("Temperature probes ok.\n");

	    writeByte(byteDef::EITPREFIX<uint8_t>);
	    write32Int(paramData.eitProbes);
	    CHECKPROBE("eit");
	    printf("EIT probes ok .\n");

	    writeByte(byteDef::ECTPREFIX<uint8_t>);
	    write32Int(paramData.ectProbes);
	    CHECKPROBE("ect");
	    printf("ECT probes ok.\n");

	    writeByte(byteDef::SPECPREFIX<uint8_t>);
	    write32Int(paramData.specProbes);
	    CHECKPROBE("spec");
	    printf("Spectroscopy sensors ok.\n");

	    writeByte(byteDef::INTERVALPREFIX<uint8_t>);
	    write32Int(paramData.interval);
	    CHECKPROBE("interval");
	    printf("Interval %d ok.\n", paramData.interval);

	    sCommData.tempData = std::make_unique<float[]>(paramData.tempProbes);
	    sCommData.eitData = std::make_unique<float[]>(paramData.eitProbes);
	    sCommData.ectData = std::make_unique<float[]>(paramData.ectProbes);
	    sCommData.specData = std::make_unique<float[]>(paramData.specProbes);


	    writeByte(ackBytes.finished);
	    CHECKPROBE("finish");
	    printf("Init successful.\n");
	    return EXIT_SUCCESS;
	}

	[[nodiscard]] inline auto dataTransfer(fpgaData::argData clArgs) noexcept -> uint8_t {
	    while(!finishFlag) {
		uint8_t sfByte = readByte();
		if(ackBytes.finished == sfByte) {
		    writeByte(ackBytes.ok);
		    finishFlag = 1;
		} else if(ackBytes.start == sfByte) {
		    writeByte(ackBytes.ok);
		    if(transferSwitch(clArgs)) {
			return EXIT_FAILURE;
		    }
		}
	    }
	    return EXIT_SUCCESS;
	}

	[[nodiscard]] inline auto transferSwitch(fpgaData::argData clArgs) noexcept -> uint8_t {
	    uint8_t cycleFlag = 0;
	    uint8_t tempIndex = 0, eitIndex = 0, ectIndex = 0, specIndex = 0;
            nlohmann::json totalJSON;
            //std::vector<uint8_t> totalBSON;
            
	    //sCommData.timeStamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            //std::chrono::high_resolution_clock::time_point tPoint = std::chrono::high_resolution_clock::now();
            std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
            std::chrono::system_clock::duration tp = now.time_since_epoch();

            tp -= std::chrono::duration_cast<std::chrono::seconds>(tp);

            time_t tt = std::chrono::system_clock::to_time_t(now);
            

	    static std::ofstream logFile;
	    logFile.open(clArgs.file, std::fstream::app | std::fstream::out);
	    while(!cycleFlag) {
		switch(readByte()) {
		case byteDef::TEMPPREFIX<uint8_t>:
		    writeByte(ackBytes.ok);
		    sCommData.tempData[tempIndex] = readFloat();
		    tempIndex++;
		    writeByte(ackBytes.ok);
		    break;
		case byteDef::EITPREFIX<uint8_t>:
		    writeByte(ackBytes.ok);
		    sCommData.eitData[eitIndex] = readFloat();
		    //std::cout << sCommData.eitData[eitIndex] << std::endl;
		    eitIndex++;
		    writeByte(ackBytes.ok);
		    break;
		case byteDef::ECTPREFIX<uint8_t>:
		    writeByte(ackBytes.ok);
		    sCommData.ectData[ectIndex] = readFloat();
		    ectIndex++;
		    writeByte(ackBytes.ok);
		    break;
		case byteDef::SPECPREFIX<uint8_t>:
		    writeByte(ackBytes.ok);
		    sCommData.specData[specIndex] = readFloat();
		    //std::cout << sCommData.specData[specIndex] << std::endl;
		    specIndex++;
		    writeByte(ackBytes.ok);
		    break;
		case 'F':
		    tempIndex = 0;
		    eitIndex = 0;
		    ectIndex = 0;
		    specIndex = 0;
                    
		    printf("Finished\n");
                    totalJSON["Timestamp"] = sCommData.timeStamp;

                    now = std::chrono::system_clock::now();
                    tp = now.time_since_epoch();

                    tp -= std::chrono::duration_cast<std::chrono::seconds>(tp);

                    tt = std::chrono::system_clock::to_time_t(now);
            
                    totalJSON["Timestamp"] = fpgaData::time_to_string(*localtime(&tt), tp);
                    //logFile << std::ctime(&sCommData.timeStamp);

		    //logFile << "Temperature:\n";
		    //logFile << clArgs.tempProbes << '\n';
                    totalJSON["Temp_p"] = clArgs.tempProbes;
		    for(uint8_t i = 0; i < clArgs.tempProbes; i++) {
			//logFile << '\t';
                        totalJSON["Temperature"].emplace_back(sCommData.tempData[i]);
			//logFile << i << " ";
			//std::cout << sCommData.tempData[i] << std::endl;
			//logFile << sCommData.tempData[i];
			//logFile << std::endl;
			//logFile.flush();
			sCommData.tempData[i] = 0;
		    }
                    
		    //logFile << "EIT:\n";
		    //logFile << clArgs.eitProbes << '\n';
                    totalJSON["EIT_p"] = clArgs.eitProbes;
		    for(uint8_t i = 0; i < clArgs.eitProbes; i++) {
			//logFile << '\t';
                        totalJSON["EIT"].emplace_back(sCommData.eitData[i]);
			//logFile << i << " ";
			//std::cout << sCommData.eitData[i] << std::endl;
                        //logFile << sCommData.eitData[i];
			//logFile << std::endl;
			//logFile.flush();
			sCommData.eitData[i] = 0;
		    }

		    //logFile << "ECT:\n";
		    //logFile << clArgs.ectProbes << '\n';
                    totalJSON["ECT_p"] = clArgs.ectProbes;
		    for(uint8_t i = 0; i < clArgs.ectProbes; i++) {
			//logFile << '\t';
                        totalJSON["ECT"].emplace_back(sCommData.ectData[i]);
			//logFile << i << " ";
			//std::cout << sCommData.ectData[i] << std::endl;
			//logFile << sCommData.ectData[i];
			//logFile << std::endl;
			//logFile.flush();
			sCommData.ectData[i] = 0;
		    }

		    //logFile << "Spectroscopy:\n";
		    //logFile << clArgs.specProbes << '\n';
                    totalJSON["Spec_p"] = clArgs.specProbes;
		    for(uint8_t i = 0; i < clArgs.specProbes; i++) {
			//logFile << '\t';
                        totalJSON["Spectroscopy"].emplace_back(sCommData.specData[i]);
			//logFile << i << " ";
			//std::cout << sCommData.specData[i] << std::endl;
			//logFile << sCommData.specData[i];
			//logFile << std::endl;
			//logFile.flush();
			sCommData.specData[i] = 0;
		    }
                    
		    sCommData.timeStamp = 0;
		    logFile << std::endl;
                    std::cout << totalJSON << std::endl;
                    logFile << totalJSON;
                    logFile.flush();
		    logFile.close();
		    cycleFlag = 1;
		    writeByte(ackBytes.ok);
		    break;
		default:
		    break;
		}
	    }
	    return EXIT_SUCCESS;
	}


	inline auto readByte() noexcept -> uint8_t {
	    recBuffer = sCommDev.read(fpgaData::PREFIXLEN<size_t>);
	    uint8_t byteBuf = static_cast<uint8_t>(recBuffer[0]);
	    recBuffer.clear();
	    return byteBuf;
	}

	inline auto readFloat() noexcept -> float {
	    union byteDef::byteFloat floatBuf;
	    recBuffer = sCommDev.read(fpgaData::SENSORLEN<size_t>);
	    for(uint8_t i = 0; i < fpgaData::SENSORLEN<uint8_t>; i++) {
		floatBuf.byteVal[i] = static_cast<uint8_t>(recBuffer[i]);
	    }
	    recBuffer.clear();
	    return floatBuf.floatVal;
	}

	inline auto writeByte(const uint8_t byte) noexcept -> void {
	    sCommDev.write(&byte, fpgaData::PREFIXLEN<size_t>);
	}

	inline auto write32Int(uint32_t arg) noexcept -> void {
	    union byteDef::byte32Int intBuf;
	    intBuf.intVal = arg;
	    sCommDev.write(intBuf.byteVal, fpgaData::PARAMLEN<size_t>);
	}

    private:
	serial::Serial sCommDev;
	fpgaData::sensorData sCommData;
	byteDef::ackBytes ackBytes;
	std::string recBuffer;
	std::string sendBuffer;
	uint8_t finishFlag;
    };
}
#endif
