#ifndef DATA
#define DATA
#include <vector>
#include <string>
#include <cstdint>
#include <chrono>
//#include <ctime>
#include <memory>
#include <sstream>
#include <iomanip>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreorder"
#pragma GCC diagnostic ignored "-Weffc++"
#include "serial/serial.h"
#pragma GCC diagnostic pop

namespace fpgaData {
    template <typename T> constexpr static T PREFIXLEN{sizeof(uint8_t)};
    template <typename T> constexpr static T SENSORLEN{sizeof(float)};
    template <typename T> constexpr static T PARAMLEN{sizeof(uint32_t)};

    typedef struct sensorData {
	sensorData() : timeStamp{}, tempData{}, eitData{}, ectData{}, specData{}
	    {}
	~sensorData()
	    {}
	std::time_t timeStamp;
	std::unique_ptr<float[]> tempData;
	std::unique_ptr<float[]> eitData;
	std::unique_ptr<float[]> ectData;
	std::unique_ptr<float[]> specData;
    } sensorData;

    typedef struct argData {
    public:
      argData()
	  : baudRate{115200}, device{"/dev/ttyUSB1"}, file{"log"}, reads{100},
	    interval{100000}, tempProbes{5}, eitProbes{5}, ectProbes{5},
	    specProbes{5} {};
      ~argData(){};
      uint32_t baudRate;
      std::string device;
      std::string file;
      uint32_t reads;
      uint32_t interval;
      uint32_t tempProbes;
      uint32_t eitProbes;
      uint32_t ectProbes;
      uint32_t specProbes;
    } argData;

    auto enumerate_ports() noexcept -> void {
	std::vector<serial::PortInfo> devices_found = serial::list_ports();
	std::vector<serial::PortInfo>::iterator iter = devices_found.begin();

	while(iter != devices_found.end()) {
		serial::PortInfo device = *iter++;

		printf("(%s, %s, %s)\n", device.port.c_str(), device.description.c_str(),
			device.hardware_id.c_str() );
	}
    }
    template <typename Duration>
        std::string time_to_string(tm t, Duration fraction) {
        using namespace std::chrono;
        std::string tmpString;
        std::stringstream ss;
        ss << std::setw(4) << std::setfill('0') << t.tm_year + 1900 << "-" << std::setw(2) << t.tm_mon + 1 << "-" << std::setw(2) << t.tm_mday << " " << std::setw(2) << t.tm_hour << ":" << t.tm_min << ":" << std::setw(2) << t.tm_sec << "." << std::setw(3) << static_cast<unsigned>(fraction / milliseconds(1));
        tmpString = ss.str();
        //tmpString = std::to_string(t.tm_year + 1900) + "-" + std::to_string(t.tm_mon + 1) + "-" + std::to_string(t.tm_mday) + " " + std::to_string(t.tm_hour) + ":" + std::to_string(t.tm_min) + ":" + std::to_string(t.tm_sec) + "." + std::to_string(static_cast<unsigned>(fraction / milliseconds(1)));
        return tmpString;
    }
}

namespace byteDef {
    template <typename T> constexpr static T TEMPPREFIX{'T'};
    template <typename T> constexpr static T EITPREFIX{'I'};
    template <typename T> constexpr static T ECTPREFIX{'C'};
    template <typename T> constexpr static T SPECPREFIX{'S'};
    template <typename T> constexpr static T INTERVALPREFIX{'V'};
    template <typename T> constexpr static T READSPREFIX{'R'};

    union byteFloat {
	float floatVal;
	uint8_t byteVal[4];
    } byteFloat;

    union byte32Int {
	uint32_t intVal;
	uint8_t byteVal[4];
    } byte32Int;

    typedef struct ackBytes {
	const uint8_t ok       = 'O';
	const uint8_t nok      = 'N';
	const uint8_t finished = 'F';
	const uint8_t start    = 'S';
    } ackBytes;
}
#endif
