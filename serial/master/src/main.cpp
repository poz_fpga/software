#include <iostream>
#include <getopt.h>
#include "../include/serialComm.hpp"

int main(int argc, char** argv) {
    fpgaData::argData argStruct;
    //byteDef::ackBytes ackBytes;
    int32_t c = 0;
    static struct option longOptions[] = {
	{"baudrate", required_argument, 0, 'b'},
	{"device", required_argument, 0, 'd'},
	{"reads", required_argument, 0, 'r'},
	{"interval", required_argument, 0, 'v'},
	{"imp_probes", required_argument, 0, 'i'},
	{"cap_probes", required_argument, 0, 'c'},
	{"spec_probes", required_argument, 0, 's'},
	{"temp_probes", required_argument, 0, 't'},
	{"ports", no_argument, 0, '0'},
	{"log_file", required_argument, 0, 'f'},
	{0, 0, 0, 0}};

    while ((c = getopt_long(argc, argv, "b:d:r:v:i:c:s:t:f:0:", longOptions, NULL)) !=-1) {
      switch (c) {
      case 'b':
	  argStruct.baudRate = static_cast<uint32_t>(std::stoul(optarg));
	  break;
      case 'd':
	  argStruct.device.assign(optarg);
	  break;
      case 'r':
	  argStruct.reads = static_cast<uint32_t>(std::stoul(optarg));
	  break;
      case 'v':
	  argStruct.interval = static_cast<uint32_t>(std::stoul(optarg));
          std::cout << argStruct.interval << std::endl;
          //argStruct.interval = 1000;
	  break;
      case 'i':
	  argStruct.eitProbes = static_cast<uint32_t>(std::stoul(optarg));
	  break;
      case 'c':
	  argStruct.ectProbes = static_cast<uint32_t>(std::stoul(optarg));
	  break;
      case 's':
	  argStruct.specProbes = static_cast<uint32_t>(std::stoul(optarg));
	  break;
      case 't':
	  argStruct.tempProbes = static_cast<uint32_t>(std::stoul(optarg));
	  break;
      case 'f':
	  argStruct.file.assign(optarg);
	  break;
      case '0':
	  fpgaData::enumerate_ports();
	  exit(EXIT_SUCCESS);
	  break;
      default:
	  break;
      }
    }
    /*
    std::cout << "Baudrate " << argStruct.baudRate <<
	"\nDevice " << argStruct.device <<
	"\nReads " << argStruct.reads <<
	"\nInterval " << argStruct.interval <<
	"\nImp probes " << argStruct.eitProbes <<
	"\nCap probes " << argStruct.ectProbes <<
	"\nTemp probes " << argStruct.tempProbes <<
	"\nLog file " << argStruct.file << std::endl;
    */

    try {
	serialComm::sCommClass comm1(argStruct.device, argStruct.baudRate, 2000);
	if(comm1.initParam(argStruct)) {
	    return EXIT_FAILURE;
	}

	if(comm1.dataTransfer(argStruct)) {
	    return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;

    } catch (std::exception &e) {
	printf("Could not open serial port, available ports are:\n");
	fpgaData::enumerate_ports();
	return EXIT_FAILURE;
	}
    return 0;
}
