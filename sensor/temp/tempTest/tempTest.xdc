# To D0_P  Pin 1 = Slave Select
set_property PACKAGE_PIN M15 [get_ports {SS[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SS[0]}]

# To D0_N  Pin 2 = MOSI (Unconnected on MAX31855)
set_property PACKAGE_PIN L15 [get_ports MOSI]
set_property IOSTANDARD LVCMOS33 [get_ports MOSI]

# To D1_P  Pin 3 = MISO
set_property PACKAGE_PIN M14 [get_ports MISO]
set_property IOSTANDARD LVCMOS33 [get_ports MISO]

# To D1_N  Pin 4 = SPI Clock
set_property PACKAGE_PIN L14 [get_ports SCK]
set_property IOSTANDARD LVCMOS33 [get_ports SCK]

set_property PULLUP true [get_ports {SS[0]}]
set_property PULLUP true [get_ports MOSI]
set_property PULLUP true [get_ports MISO]
set_property PULLUP true [get_ports SCK]
