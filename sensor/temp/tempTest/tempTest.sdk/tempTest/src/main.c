#include "xparameters.h"	/* XPAR parameters */
#include "xspi.h"		/* SPI device driver */
#include "xspi_l.h"
#include "sleep.h"
#include "xil_printf.h"

/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID

/*
 *  This is the size of the buffer to be transmitted/received in this example.
 */
#define BUFFER_SIZE		4

//typedef u8 DataBuffer[BUFFER_SIZE];

typedef union spiData {
	u8 byteBuffer[BUFFER_SIZE];
	u32 intData;
} spiData;

spiData ReadBuffer;
spiData WriteBuffer;


/***************** Macros (Inline Functions) Definitions *********************/


/************************** Function Prototypes ******************************/

int SpiPolledExample(XSpi *SpiInstancePtr, u16 SpiDeviceId);

/************************** Variable Definitions *****************************/

/*
 * The instances to support the device drivers are global such that the
 * are initialized to zero each time the program runs.
 */
static XSpi  SpiInstance;	 /* The instance of the SPI device */


/*****************************************************************************/
/**
*
* Main function to call the Spi Polled example.
*
* @param	None
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note		None
*
******************************************************************************/
int main(void)
{
	int Status;

	/*
	 * Run the Spi Polled example.
	 */
	xil_printf("Starting\n\r");
	Status = SpiPolledExample(&SpiInstance, SPI_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Spi polled Example Failed\r\n");
		return XST_FAILURE;
	}

	xil_printf("Successfully ran Spi polled Example\r\n");
	return XST_SUCCESS;
}

/*****************************************************************************/
/**
*
* This function does a minimal test on the Spi device and driver as a
* design example. The purpose of this function is to illustrate how to use
* the XSpi component using the polled mode.
*
* This function sends data and expects to receive the same data.
*
*
* @param	SpiInstancePtr is a pointer to the instance of Spi component.
* @param	SpiDeviceId is the Device ID of the Spi Device and is the
*		XPAR_<SPI_instance>_DEVICE_ID value from xparameters.h.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note
*
* This function contains an infinite loop such that if the Spi device is not
* working it may never return.
*
******************************************************************************/
int SpiPolledExample(XSpi *SpiInstancePtr, u16 SpiDeviceId)
{
	int Status;
	XSpi_Config *ConfigPtr;	/* Pointer to Configuration data */

	/*
	 * Initialize the SPI driver so that it is  ready to use.
	 */
	ConfigPtr = XSpi_LookupConfig(SpiDeviceId);
	if (ConfigPtr == NULL) {
		return XST_DEVICE_NOT_FOUND;
	}

	Status = XSpi_CfgInitialize(SpiInstancePtr, ConfigPtr,
				  ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built correctly.
	 */
	Status = XSpi_SelfTest(SpiInstancePtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	if (SpiInstancePtr->SpiMode != XSP_STANDARD_MODE) {
		return XST_SUCCESS;
	}

	/*
	 * Set the Spi device as a master
	 */
	// XSP_CLK_PHASE_1_OPTION
	// XSP_CLK_ACTIVE_LOW_OPTION
	Status = XSpi_SetOptions(SpiInstancePtr, XSP_MASTER_OPTION | XSP_MANUAL_SSELECT_OPTION);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	/*
	 * Start the SPI driver so that the device is enabled.
	 */
	XSpi_Start(SpiInstancePtr);

	xil_printf("%d\n\r", XSpi_GetStatusReg(SpiInstancePtr));

	XSpi_SetSlaveSelect(SpiInstancePtr, 0x01);

	float temp = 0;
	u32 tempVar = 0;
	u8 emptyBuf[BUFFER_SIZE];
	for(u8 i = 0; i < BUFFER_SIZE; i++) {
		emptyBuf[i] = 0;
	}
	while(1) {
		WriteBuffer.intData = 0xFFFFFFFF;
		ReadBuffer.intData = 0;
		temp = 0;
		tempVar = 0;
		XSpi_IntrGlobalDisable(SpiInstancePtr);
		XSpi_Transfer(SpiInstancePtr, emptyBuf, ReadBuffer.byteBuffer, BUFFER_SIZE);
		XSpi_IntrGlobalEnable(SpiInstancePtr);
		/*
		for(u8 i =0; i < 32; i++) {
			if((ReadBuffer.intData >> (32-i)) & 1) {
				xil_printf("%d", 1);
			} else {
				xil_printf("%d", 0);
			}
		}
		*/
		xil_printf("\n\r");
		tempVar = ReadBuffer.intData >> 18;
		for(u8 i =0; i < 14; i++) {
			if((tempVar >> (14-i)) & 1) {
				xil_printf("%d", 1);
			} else {
				xil_printf("%d", 0);
			}
		}
		xil_printf("\n\r");
		for(int8_t i = 0; i < 14; i++) {
			//temp += powf(((tempVar >> i) & 1) * 2, (float)((int)i - 2));
			temp += (0.25 * (tempVar & (1 << i)));
		}
		xil_printf("Temp is %d\n\r", (u32)temp);
		xil_printf("\n\r");
		usleep_A9(500000);
	}

	return XST_SUCCESS;
}
