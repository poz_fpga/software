#include <stdio.h>
#include "xil_io.h"
#include "ECT_concept.h"
#include "platform.h"
#include "xil_printf.h"
#include "sleep.h"

#define ECT_BASE_ADDR 0x43c00000
#define CLK_FREQ      50000000u


int main()
{
    init_platform();

    xil_printf("Starting ECT measurement\n\r");
    u32 inputBuffer = 0;
    u32 clockMeasurement = 0;

    while(1) {

    	ECT_CONCEPT_mWriteReg(ECT_BASE_ADDR, ECT_CONCEPT_S00_AXI_SLV_REG1_OFFSET, (1<<28));

    	// Wait for measurement to finish
    	/*while(((inputBuffer = ECT_CONCEPT_mReadReg(ECT_BASE_ADDR, ECT_CONCEPT_S00_AXI_SLV_REG0_OFFSET))
    			&& 0x80000000) == 0) {
    		//xil_printf("Input buffer: %x\n\r", inputBuffer);
    	}*/

    	while((inputBuffer = ECT_CONCEPT_mReadReg(ECT_BASE_ADDR,
    						 ECT_CONCEPT_S00_AXI_SLV_REG0_OFFSET)) == 0) {
    		xil_printf("Input buffer: %x\n\r", inputBuffer);v
    	}

    	xil_printf("Input buffer: %x\n\r", inputBuffer);
    	ECT_CONCEPT_mWriteReg(ECT_BASE_ADDR, ECT_CONCEPT_S00_AXI_SLV_REG1_OFFSET, 0);
    	clockMeasurement = inputBuffer & 0x0FFFFFFF;
    	xil_printf("Clock cycles: %u\n\r", clockMeasurement);

    	sleep_A9(3);


    }
    cleanup_platform();
    return 0;
}
