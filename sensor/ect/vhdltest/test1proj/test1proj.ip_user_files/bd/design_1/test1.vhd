library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ECT is
  port (
    enable             : in  std_logic;                         -- enable measurement
    finish_measurement : in  std_logic;                         -- output of comparator
    clk                : in  std_logic;                         -- clock input
    step_gen           : out std_logic;                         -- step on capacitor
    output_ready       : out std_logic;                         -- high when measurement is finished
    count              : out std_logic_vector(27 downto 0));    -- clock counter
end entity ECT;
architecture behavior of ECT is
  signal internal_count : std_logic_vector(27 downto 0) := x"0000000";  -- internal counter
  -- signal enable_flag    : std_logic                     := '1';         -- enable
  
begin  -- architecture behavior
  
  -- enable_counter : process (enable) is
  -- begin  -- process enable_counter
  --   if (enable = '1') then
  --     enable_flag <= '1';
  --   else
  --     enable_flag    <= '0';
  --     internal_count <= x"0000000";
  --     count          <= x"0000000";
  -- end if;
  -- end process enable_counter;

  -- purpose: count clock pulses
  -- type   : sequential
  -- inputs : clk
  -- outputs: count
  counter : process (clk, enable) is
  begin  -- process counter
    if (clk'event and clk = '1') then  -- rising clock edge
      if (enable = '1') then
        internal_count <= std_logic_vector(unsigned(internal_count) + 1);
      else
        internal_count <= x"0000000";
      end if; 
    end if;
  end process counter;
  
  step_gen <= enable;
  output_ready <= '1' when (internal_count > x"02FAF080") and enable = '1' else
                  finish_measurement when enable = '1' else
                  '0';

  count <= internal_count when finish_measurement = '1' else
           x"0000000";
end architecture behavior;
