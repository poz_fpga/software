set_property PACKAGE_PIN R8 [get_ports finish_measurement_0]
set_property PACKAGE_PIN P8 [get_ports step_gen_0]
set_property IOSTANDARD LVCMOS33 [get_ports finish_measurement_0]
set_property IOSTANDARD LVCMOS33 [get_ports step_gen_0]
